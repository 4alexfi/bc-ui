import React from "react";
import styled from "styled-components";
import Types from "prop-types";
import { colors } from "./styles";

const Wrapper = styled.p`
  font-size: 13px;
  line-height: 1.38;
  text-align: left;
  color: ${colors.black};
`;

const ShowMore = styled.a`
  color: ${colors.silver};
  text-decoration: underline;
  cursor: pointer;

  &:hover {
    text-decoration: none;
  }
`;

export default class DiscrolsureText extends React.Component {

    static propTypes = {
        children:       Types.string.isRequired,
        characterLimit: Types.number,
        showText:       Types.string,
        hideText:       Types.string,
    };

    static defaultProps = {
        showText:       "Show more",
        hideText:       "Show less",
        characterLimit: 300,
    }

    constructor(props) {
        super(props);
        this.state = { opened: false };

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            opened: !this.state.opened,
        });
    }

    render() {
        const { children, characterLimit, showText, hideText } = this.props;
        const { opened } = this.state;

        if (children.length > characterLimit) {
            return (
                <Wrapper onClick={this.toggle}>
                    { opened ? children : `${children.substring(0, characterLimit)}... ` }
                    <ShowMore onClick={this.toggle}>{ opened ? hideText : showText }</ShowMore>
                </Wrapper>
            );
        }
        return (<Wrapper>{ children }</Wrapper>);
    }

}
