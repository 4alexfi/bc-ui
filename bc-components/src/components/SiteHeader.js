import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { Grid } from "grid-styled";
import PropTypes from "prop-types";

import { colors, mediaMax, Thumb } from "./shared";
import {
    EditDark,
    NotificationsDark,
    MoreVerticalDark,
    ShareDark,
    PlusDark,
    LogoHeader,
    SearchDark,
} from "../icons";

const Wrapper = styled(Grid)`
    padding: 9px;
    z-index: 999;
    position: fixed;
    width: 100%;
    top: 0;
    left: 0;
    background: ${colors.alphaWhite};
    box-sizing: border-box;

    ${mediaMax.m`
        display: none;
    `};
`;

const Icon = styled.div`
    width: 40px;
    height: 40px;
    display: inline-block;
    background-image: ${({ src }) => (`url(${src})`)};
    background-position: center;
    background-repeat: no-repeat;
`;

const LeftButtonsWrapper = styled(Grid)`
    vertical-align: middle;
`;

const RightButtonsWrapper = styled(Grid)`
    text-align: right;
    vertical-align: middle;
`;

const CenterButtonsWrapper = styled(Grid)`
    vertical-align: middle;
    text-align: center;
`;

const Userpic = Thumb.extend`
    margin: 5px 0;
`;

const HeaderLink = styled(Link)`
    text-align: center;
    vertical-align: middle;
    display: inline-block;
    width: 40px;
    height: 40px;
    margin: 0 5px;

    -webkit-transition: opacity 0.15s linear;
    -moz-transition: opacity 0.15s linear;
    transition: opacity 0.15s linear;

    &:hover {
        opacity: 0.5;
    }
`;

export const HeaderContent = styled.div`
    &:first-child {
        margin-left: 0px;
    }
`;

const SiteHeader = ({ leftContent, centerContent, rightContent }) => (
    <Wrapper>
        <LeftButtonsWrapper width={5 / 14}>
            { leftContent }
        </LeftButtonsWrapper>
        <CenterButtonsWrapper width={4 / 14}>
            { centerContent }
        </CenterButtonsWrapper>
        <RightButtonsWrapper width={5 / 14}>
            { rightContent }
        </RightButtonsWrapper>
    </Wrapper>
);

SiteHeader.propTypes = {
    leftContent:   PropTypes.shape.isRequired,
    centerContent: PropTypes.shape.isRequired,
    rightContent:  PropTypes.shape.isRequired,
};

SiteHeader.defaultProps = {
    leftContent:   null,
    centerContent: null,
    rightContent:  null,
};

export default SiteHeader;

const HeaderButton = ({ action }, icon) => (
    <HeaderLink to={action}><Icon src={icon} /></HeaderLink>
);
HeaderButton.propTypes = {
    action: PropTypes.string.isRequired,
};

export const ButtonEdit = props => HeaderButton(props, EditDark);
export const ButtonAdd = props => HeaderButton(props, PlusDark);
export const ButtonShare = props => HeaderButton(props, ShareDark);
export const ButtonSearch = props => HeaderButton(props, SearchDark);
export const ButtonMoreVertical = props => HeaderButton(props, MoreVerticalDark);
export const ButtonNotifications = props => HeaderButton(props, NotificationsDark);

export const ButtonLogo = props => HeaderButton(props, LogoHeader);

export const ButtonUserpic = ({ action, picture }) => (
    <HeaderLink to={action}><Userpic picture={picture} rounded size={27} /></HeaderLink>
);

ButtonUserpic.propTypes = {
    action:  PropTypes.string.isRequired,
    picture: PropTypes.string.isRequired,
};
