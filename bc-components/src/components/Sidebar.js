import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

import { colors, mediaMax } from "./shared";

const SIDEBAR_WIDTH = 440;

const SidebarWrapper = styled.div`
    position: fixed;
    background: ${colors.sidebarGrey};
    top: 0;
    right: -${({ isVisible }) => (isVisible ? 0 : SIDEBAR_WIDTH)}px;
    height: 100%;
    width: ${SIDEBAR_WIDTH}px;
    z-index: 999;
    padding: 0 30px 30px 30px;
    box-sizing: border-box;
    overflow-y: scroll;

    -webkit-transition: right 200ms cubic-bezier(0.19, 1, 0.22, 1);
    transition:         right 200ms cubic-bezier(0.19, 1, 0.22, 1);

    ${mediaMax.m`
        display: none;
    `}
`;


const Sidebar = ({ children, isVisible }) => (
    <SidebarWrapper isVisible={isVisible}>
        {children}
    </SidebarWrapper>
);

Sidebar.propTypes = {
    children:  PropTypes.shape.isRequired,
    isVisible: PropTypes.bool,
};

Sidebar.defaultProps = {
    isVisible: false,
};

export default Sidebar;
