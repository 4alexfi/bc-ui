import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { colors, media } from "./shared";

const Wrapper = styled.div`
    padding: 10px;
`;

const ContentWrapper = styled.div`
    padding-left: 32px;
    vertical-align: top;

    ${media.m`
        padding-left: 44px;
    `}
`;

const Userpic = styled.div`
    float: left;
    vertical-align: top;
    width: 22px;
    height: 22px;
    border-radius: 11px;
    background-image: url(${({ picture }) => picture});
    background-repeat: no-repeat;
    background-color: #f7f7f7;
    background-size: contain;

    ${media.m`
        width: 24px;
        height: 24px;
    `}
`;

const DateLabel = styled.label`
    font-size: 11px;
    color: #bbb;
    display: block;
    margin-top: 10px;

    ${media.m`
        font-size: 12px;
    `}
`;

const NotificationText = styled.p`
    margin: 0;
    padding: 0;
    font-size: 12px;
    line-height: 1.3;
    color: ${colors.black};
    ${media.m`
        font-size: 14px;
    `}
`;

const Notification = ({ notification: { picture, date, author, action, title } }) => (
    <Wrapper>
        <Userpic picture={picture} />
        <ContentWrapper>
            <NotificationText>
                <b>{author}</b> {action} <b>{title}</b>
            </NotificationText>
            <DateLabel>{date}</DateLabel>
        </ContentWrapper>
    </Wrapper>
);

Notification.propTypes = {
    notification: PropTypes.shape({
        picture:  PropTypes.string,
        author:   PropTypes.string.isRequired,
        date:     PropTypes.string.isRequired,
        action:   PropTypes.string.isRequired,
        title:    PropTypes.string.isRequired,
    }).isRequired,
};

export default Notification;
