import styled from "styled-components";

import { colors, media } from "./Styles";

export const h1 = styled.h1`
    font-size: 22px;
    font-weight: bold;
    line-height: 1.36;
    text-align: left;
    color: ${colors.black};
`;

export const h4 = styled.h4`
    font-size: 13px;
    line-height: 1.62;
    font-weight: 500;
    text-align: left;
    color: ${colors.black};

    ${media.l`
        font-size: 16px;
        line-height: 1.31;
    `}
`;

export const BlockTitle = styled.div`
    height: 15px;
    font-size: 11px;
    font-weight: bold;
    line-height: 1.36;
    letter-spacing: 0.5px;
    text-align: left;
    color: ${colors.silver};
    text-transform: ${props => (props.uppercase ? "uppercase" : "none")};
`;

export const Thumb = styled.div`
  width: ${props => props.size}px;
  height: ${props => props.size}px;
  display: inline-block;
  border-radius: ${({ rounded = false, size = 0 }) => (rounded ? size / 2 : 0)}px;
  background-color: ${colors.paleGrey};
  background-image: ${props => `url(${props.picture})`};
  background-size: contain;
`;
