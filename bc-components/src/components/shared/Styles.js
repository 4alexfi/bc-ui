import { css, injectGlobal } from "styled-components";

export const colors = {
    white:         "#ffffff",
    alphaWhite:    "rgba(255, 255, 255, 0.95)",
    black:         "#121212",
    buff:          "#fff1a8",
    watermelon:    "#f74e63",
    silver:        "#b9bbbb",
    paleGrey:      "#f2f4f5",
    paleGreyTwo:   "#f7f9fa",
    sidebarGrey:   "#F2F6F7",
    lightBlueGrey: "#a2ded7",
    perrywinkle:   "#91a7e3",
    algaeGreen:    "#23ce6b",
    violet:        "#bd0fe1",
    silverTwo:     "#e4e5e6",
    inactiveTab:   "#ADB0B1",
};

export const fonts = {
    fontFamily: "'Roboto'",
};

export const breakpoints = {
    m:  768,
    l:  1280,
    xl: 1600,
};

export const mediaMax = Object.keys(breakpoints).reduce((acc, label) => {
    acc[label] = (...args) => css`
    @media (max-width: ${breakpoints[label] / 16}em) {
        ${css(...args)}
    }
  `;

    return acc;
}, {});

export const media = Object.keys(breakpoints).reduce((acc, label) => {
    acc[label] = (...args) => css`
    @media (min-width: ${breakpoints[label] / 16}em) {
      ${css(...args)}
    }
  `;

    return acc;
}, {});

// eslint-disable-next-line no-unused-expressions
injectGlobal`
   html,
   body {
       margin: 0;
       padding: 0;
       height: 100%;
       width: 100%;
       font-family: ${fonts.fontFamily};
   }

    #root {
        height: 100%;

    }`;
