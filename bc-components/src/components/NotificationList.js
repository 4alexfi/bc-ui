import React, { Component } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import PropTypes from "prop-types";

import Notification from "./Notification";
import { colors, media } from "./shared";


const Wrapper = styled.div``;

const TabsWrapper = styled.div`
    text-align: center;
    padding: 30px 10px;
    display: block;
`;

const Tab = styled(Link)`
    font-weight: 700;
    text-transform: uppercase;
    font-size: 10px;
    color: ${({ isActive }) => (isActive ? colors.black : colors.inactiveTab)};
    text-decoration: none;

    &:hover {
        text-decoration: underline;
    }

    ${media.m`
        font-size: 11px;
        padding: 20px 10px;
    `}
`;

const ListWrapper = styled.ul`
    list-style: none;
    padding: 0;
    margin: 0;

    &:last-child {
        margin-bottom: 0;
    }
`;

const NotificationWrapper = styled.li`
    ${media.m`
        margin-bottom: 10px;
    `}
`;

const NotificationLink = styled(Link)`
    text-decoration: none;
    color: ${colors.black};
    &:hover > p {
        text-decoration: underline;
    }
`;


class NotificationList extends Component {

    _renderNotifications() {
        const { notifications } = this.props;

        return notifications.map(n => (
            <NotificationLink to={n.link} key={`NotificationItem__${n.id}`}>
                <NotificationWrapper>
                    <Notification notification={n} />
                </NotificationWrapper>
            </NotificationLink>
        ));
    }

    _renderTabs() {
        const { activeTab, tabs } = this.props;

        return (
            <TabsWrapper>
                { tabs.map(({ link, title }, index) => <Tab to={link} isActive={index === activeTab}>{title}</Tab>) }
            </TabsWrapper>
        );
    }

    render() {
        return (
            <Wrapper>
                { this._renderTabs() }
                <ListWrapper>
                    { this._renderNotifications() }
                </ListWrapper>
            </Wrapper>
        );
    }
}


NotificationList.propTypes = {
    activeTab: PropTypes.number.isRequired,
    tabs:      PropTypes.arrayOf(PropTypes.shape({
        link:  PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
    })).isRequired,
    notifications: PropTypes.arrayOf(PropTypes.shape({
        link:    PropTypes.string.isRequired,
        userpic: PropTypes.string,
        author:  PropTypes.string.isRequired,
        date:    PropTypes.string.isRequired,
        action:  PropTypes.string.isRequired,
        title:   PropTypes.string.isRequired,
    })).isRequired,
};

NotificationList.defaultProps = {
    activeTab: 0,
    tabs:      [],
};

export default NotificationList;
