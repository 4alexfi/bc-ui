import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import AppHeader from "./AppHeader";
import registerServiceWorker from "./registerServiceWorker";

import {
    BrowserRouter as Router,
} from "react-router-dom";

ReactDOM.render(<Router><AppHeader /></Router>, document.getElementById("root"));
registerServiceWorker();
