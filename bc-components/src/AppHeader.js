import React from "react";
import styled from "styled-components";

import SiteHeader, {
    ButtonSearch,
    HeaderContent,
    ButtonLogo,
    ButtonShare,
    ButtonEdit,
    ButtonMoreVertical,
    ButtonNotifications,
    ButtonUserpic,
} from "./components/SiteHeader";
import Sidebar from "./components/Sidebar";
import NotificationList from "./components/NotificationList";
import Avatar from "./avatar.png";


const AppWrapper = styled.div`
  padding: 20px;
  margin: 0 auto;
  font-family: "Roboto";
`;

const App = () => (
    <AppWrapper>
        <SiteHeader
            leftContent={
                <HeaderContent>
                    <ButtonLogo action="/logo" />
                </HeaderContent>
            }
            centerContent={
                <HeaderContent>
                    <ButtonShare action="/logo" />
                    <ButtonMoreVertical action="/logo" />
                    <ButtonEdit action="/logo" />
                </HeaderContent>
            }
            rightContent={
                <HeaderContent>
                    <ButtonSearch action="/logo" />
                    <ButtonNotifications action="/logo" />
                    <ButtonUserpic
                        action="/logo"
                        picture={Avatar}
                    />
                </HeaderContent>
            }
        />
        <Sidebar isVisible>
        <NotificationList
            activeTab={ 0 }
            tabs={
                [
                    {
                        link: "/notifications",
                        title: "Notifications",
                    },
                    {
                        link: "/notifications/evidences",
                        title: "Evidences",
                    },
                ]
        }
                        notifications = {[
                            { id:     11, author: "Šarūnas Davalga", action: "joined", title:  "Certbot renewal - restart nginx!", date:   Date(), link: "/project/cool", picture: Avatar, }, { id:     21, author: "Šarūnas Davalga", action: "joined", title:  "Certbot renewal - restart nginx!", date:   Date(), link: "/project/cool", picture: Avatar, }, { id:     31, author: "Šarūnas Davalga", action: "joined", title:  "Certbot renewal - restart nginx!", date:   Date(), link: "/project/cool", picture: Avatar, }, { id:     41, author: "Šarūnas Davalga", action: "joined", title:  "Certbot renewal - restart nginx!", date:   Date(), link: "/project/cool", picture: Avatar, }, { id:     51, author: "Šarūnas Davalga", action: "joined", title:  "Certbot renewal - restart nginx!", date:   Date(), link: "/project/cool", picture: Avatar, }, { id:     61, author: "Šarūnas Davalga", action: "joined", title:  "Certbot renewal - restart nginx!", date:   Date(), link: "/project/cool", picture: Avatar, }, { id:     1, author: "Šarūnas Davalga", action: "joined", title:  "Certbot renewal - restart nginx!", date:   Date(), link: "/project/cool", picture: Avatar, }, { id:     2, author: "Šarūnas Davalga", action: "joined", title:  "Certbot renewal - restart nginx!", date:   Date(), link: "/project/cool", picture: Avatar, }, { id:     3, author: "Šarūnas Davalga", action: "joined", title:  "Certbot renewal - restart nginx!", date:   Date(), link: "/project/cool", picture: Avatar, }, { id:     4, author: "Šarūnas Davalga", action: "joined", title:  "Certbot renewal - restart nginx!", date:   Date(), link: "/project/cool", picture: Avatar, }, { id:     5, author: "Šarūnas Davalga", action: "joined", title:  "Certbot renewal - restart nginx!", date:   Date(), link: "/project/cool", picture: Avatar, }, { id:     6, author: "Šarūnas Davalga", action: "joined", title:  "Certbot renewal - restart nginx!", date:   Date(), link: "/project/cool", picture: Avatar, }, ]}
    />
    </Sidebar>
    </AppWrapper>
);

export default App;
