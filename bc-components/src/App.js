import React, { Component } from 'react';
import NotificationList from './components/NotificationList';
import Avatar from './avatar.png';

import styled from 'styled-components';

const AppWrapper = styled.div`
  padding: 20px;
  margin: 0 auto;
  width: 300px;
  font-family: "Roboto";
`;

class App extends Component {
  render() {
    return (
      <AppWrapper>
        <NotificationList
          notifications={
            [
              {
                id:     1,
                author: "Šarūnas Davalga",
                action: "joined",
                title:  "Certbot renewal - restart nginx!",
                date:   Date(),
                link: "/project/cool",
                picture: Avatar,
              },
              {
                id:     2,
                author: "Šarūnas Davalga",
                action: "joined",
                title:  "Certbot renewal - restart nginx!",
                date:   Date(),
                link: "/project/cool",
                picture: Avatar,
              },
              {
                id:     3,
                author: "Šarūnas Davalga",
                action: "joined",
                title:  "Certbot renewal - restart nginx!",
                date:   Date(),
                link: "/project/cool",
                picture: Avatar,
              },
              {
                id:     4,
                author: "Šarūnas Davalga",
                action: "joined",
                title:  "Certbot renewal - restart nginx!",
                date:   Date(),
                link: "/project/cool",
                picture: Avatar,
              },
              {
                id:     5,
                author: "Šarūnas Davalga",
                action: "joined",
                title:  "Certbot renewal - restart nginx!",
                date:   Date(),
                link: "/project/cool",
                picture: Avatar,
              },
              {
                id:     6,
                author: "Šarūnas Davalga",
                action: "joined",
                title:  "Certbot renewal - restart nginx!",
                date:   Date(),
                link: "/project/cool",
                picture: Avatar,
              },
            ]
          }
        />
      </AppWrapper>
    );
  }
}

export default App;
