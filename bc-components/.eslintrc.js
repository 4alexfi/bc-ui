module.exports = {
    parserOptions: {
        ecmaVersion:  7,
        ecmaFeatures: {
            impliedStrict:                true,
            jsx:                          true,
            experimentalObjectRestSpread: true
        },
        sourceType:   'module'
    },
    parser:        'babel-eslint',
    env:           {
        node: true,
        es6:  true
    },
    globals:       {
        __DEV__:    true,
        __PROD__:   true,
        __SERVER__: true,
        __CLIENT__: true,
        __API__:    true,
    },
    extends:       "airbnb",
    "rules":       {
        "max-len":                           ["warn", 120],
        "indent":                            ["error", 4, { "SwitchCase": 1 }],
        "linebreak-style":                   ["error", "unix"],
        "quotes":                            ["error", "double"],
        "semi":                              ["error", "always"],
        "no-console":                        ["error", { allow: ["error"] }],
        "key-spacing":                       ["error", {
            "singleLine": {
                "beforeColon": false,
                "afterColon":  true,
            },
            "multiLine":  {
                "align": "value",
                "mode":  "minimum",
            }
        }],
        "no-underscore-dangle":              [0],
        "react/jsx-indent":                  ["error", 4],
        "react/jsx-indent-props":            ["error", 4],
        "react/jsx-filename-extension":      [0],
    },
};
